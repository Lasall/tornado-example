#!/usr/bin/env python3
from urllib.request import urlopen, Request
from urllib.parse import quote
from tornado import websocket, web, ioloop, gen, escape
import json
import os

cl = []

class Project:
    def __init__(self, x, y, z):
        pass

    def getId(self):
        pass

    def getName(self):
        pass

    def getSha(self):
        pass

    def getShaUrl(self):
        pass

    def getProjectUrl(self):
        pass

    def getPipelinesUrl(self):
        pass

    def getStatusIcon(self):
        pass

class IndexHandler(web.RequestHandler):
    def initialize(self):
        scriptdir = os.path.dirname(os.path.realpath(__file__))
        with open(os.path.join(scriptdir, "config.json")) as f:
            config = json.load(f)
        apiurl = config['url'] + "/api/v4/"
        apitoken = config['token']

        self.projects = []
        for project in config['projects']:
            url = apiurl + "projects/" + quote(project, safe='') + "/pipelines"
            # print(url)
            req = Request(url, headers = {"PRIVATE-TOKEN": apitoken})
            json_raw = urlopen(req).read()
            # print(json_raw)
            pipelines = json.loads(json_raw)
            for pipeline in pipelines:
                print(pipeline)
            # self.projects.append(Project(x, y, z))

    def get(self):
        self.render("index.html", projects=self.projects)

class SocketHandler(websocket.WebSocketHandler):
    def check_origin(self, origin):
        return True

    def open(self):
        if self not in cl:
            cl.append(self)

    def on_close(self):
        if self in cl:
            cl.remove(self)

class ApiHandler(web.RequestHandler):

    @gen.coroutine
    def get(self, *args):
        self.finish()
        id = self.get_argument("id")
        value = self.get_argument("value")
        data = {"id": id, "value" : value}
        data = json.dumps(data)
        for c in cl:
            c.write_message(data)

    @gen.coroutine
    def post(self):
        data = escape.json_decode(self.request.body)
        if data['object_kind'] != "pipeline":
            return
        print(data)
        return ""

app = web.Application([
    (r'/', IndexHandler),
    (r'/ws', SocketHandler),
    (r'/api', ApiHandler),
    (r'/(favicon.png)', web.StaticFileHandler, {'path': './'}),
    # (r'/(rest_api_example.png)', web.StaticFileHandler, {'path': './'}),
    (r'/images/(.*)', web.StaticFileHandler, {'path': './images/'}),
    # (r'/projects/(.*)', web.StaticFileHandler, {'path': './projects/'}),
])

if __name__ == '__main__':
    app.listen(8888)
    ioloop.IOLoop.instance().start()
